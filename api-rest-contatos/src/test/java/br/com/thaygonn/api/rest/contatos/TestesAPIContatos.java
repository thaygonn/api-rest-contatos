package br.com.thaygonn.api.rest.contatos;

import static io.restassured.RestAssured.given;

import java.util.Random;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class TestesAPIContatos {
	
	private static String requestBodyPost = "{\n" +
            "  \"nome\": \" " + (char) (new Random().nextInt(54) + 'a') + " \",\n" +
            "  \"telefone\": \"+55(11)9-5811-0636\",\n" +
            "  \"email\": \"email@testerestassured.com\",\n" +
            "  \"dataNascimento\": \"12/03/1995\" " +
            "\n}";
	
	private static String requestBodyPut = "{\n" +
            "  \"nome\": \" " + (char) (new Random().nextInt(54) + 'a') + " \",\n" +
            "  \"telefone\": \"+55(11)9-5811-0636\",\n" +
            "  \"email\": \"email@testerestassured.com\",\n" +
            "  \"dataNascimento\": \"12/03/1995\" " +
            "\n}";

	@BeforeAll
	public static void setup() {
		RestAssured.baseURI = "http://localhost:8080/api-contatos";
	}

	@Test
	public void getRequestTodosContatos() {
		Response response = given().contentType(ContentType.JSON).when().get("/contatos").then().extract().response();

		Assertions.assertEquals(200, response.statusCode());
		Assertions.assertEquals(" m ", response.jsonPath().getString("nome[0]"));
	}
	
	@Test
	public void getRequestWithQueryParam() {
		Response response = given().contentType(ContentType.JSON).param("nome", " m ").when().get("/contatos/nome").then().extract().response();

		Assertions.assertEquals(200, response.statusCode());
		Assertions.assertEquals(" m ", response.jsonPath().getString("nome[0]"));
		Assertions.assertEquals("124", response.jsonPath().getString("id[0]"));
		Assertions.assertEquals("+55(11)9-5811-0636", response.jsonPath().getString("telefone[0]"));
		Assertions.assertEquals("email@testerestassured.com", response.jsonPath().getString("email[0]"));
		Assertions.assertEquals("12/03/1995", response.jsonPath().getString("dataNascimento[0]"));
	}
	
	@Test
    public void postRequestEDeleteRequest() {
        Response responsePost = given()
                .header("Content-type", "application/json")
                .and()
                .body(requestBodyPost)
                .when()
                .post("/contatos")
                .then()
                .extract().response();
        

        Assertions.assertEquals(201, responsePost.statusCode());
        Assertions.assertEquals("+55(11)9-5811-0636", responsePost.jsonPath().getString("telefone"));
        Assertions.assertEquals("email@testerestassured.com", responsePost.jsonPath().getString("email"));
        Assertions.assertEquals("12/03/1995", responsePost.jsonPath().getString("dataNascimento"));
        Assertions.assertNotNull(responsePost.jsonPath().getString("nome"));
        Assertions.assertNotNull(responsePost.jsonPath().getString("id"));
        
        String id = responsePost.jsonPath().getString("id");
        
        Response responseDelete = given()
                .header("Content-type", "application/json")
                .when()
                .delete("/contatos/"+ id)
                .then()
                .extract().response();
        Assertions.assertEquals(200, responseDelete.statusCode());
    }
	
    @Test
    public void putRequest() {
        Response response = given()
                .header("Content-type", "application/json")
                .and()
                .body(requestBodyPut)
                .when()
                .put("/contatos/34")
                .then()
                .extract().response();

        Assertions.assertEquals(200, response.statusCode());
        Assertions.assertEquals("+55(11)9-5811-0636", response.jsonPath().getString("telefone"));
        Assertions.assertEquals("email@testerestassured.com", response.jsonPath().getString("email"));
        Assertions.assertEquals("12/03/1995", response.jsonPath().getString("dataNascimento"));
        Assertions.assertEquals("34",response.jsonPath().getString("id"));
        Assertions.assertNotNull(response.jsonPath().getString("nome"));
    }
    
}
