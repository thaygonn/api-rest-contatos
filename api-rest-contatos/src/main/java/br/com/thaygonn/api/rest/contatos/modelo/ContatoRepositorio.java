package br.com.thaygonn.api.rest.contatos.modelo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Repositório da classe Contato
 * 
 * @author Thaygonn
 *
 */
public interface ContatoRepositorio extends JpaRepository<Contato, Long> {

	@Query("SELECT c FROM Contato c WHERE c.nome = :nome")
	List<Contato> buscaContatosPorNome(@Param("nome") String nome);

}
