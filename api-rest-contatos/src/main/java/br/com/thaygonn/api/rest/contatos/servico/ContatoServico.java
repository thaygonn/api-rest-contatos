package br.com.thaygonn.api.rest.contatos.servico;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.thaygonn.api.rest.contatos.modelo.Contato;
import br.com.thaygonn.api.rest.contatos.modelo.ContatoRepositorio;

/**
 * Classe de serviço responsável por realizar o CRUD do Contato
 * 
 * @author Thaygonn
 *
 */
@Service
public class ContatoServico {

	@Autowired
	private ContatoRepositorio contatoRepositorio;

	/**
	 * Método do serviço que busca por todos os contatos e retorna uma lista de
	 * contatos
	 * 
	 * @return List<Contato>
	 */
	public List<Contato> buscaTodosContatos() {

		return contatoRepositorio.findAll();

	}

	/**
	 * Método do serviço que busca contato por id
	 * 
	 * @return Optional<Contato>
	 */
	public Optional<Contato> buscaContatoPorId(Long id) {

		return contatoRepositorio.findById(id);

	}

	/**
	 * Método do serviço que busca por todos os contatos que tenham o mesmo nome
	 * 
	 * @return List<Contato>
	 */
	public List<Contato> buscaContatoPorNome(String nome) {
		return contatoRepositorio.buscaContatosPorNome(nome);

	}

	/**
	 * Método do serviço que adiciona contato no banco de dados e retorna o contato
	 * adicionado
	 * 
	 * @param contato
	 * @return Contato adicionado
	 */
	public Contato adiciona(Contato contato) {

		return contatoRepositorio.save(contato);

	}

	/**
	 * Método do serviço que atualiza informações do contato "original" que vem do
	 * banco com as informações vindas na requisição
	 * 
	 * @param Contato atualizado
	 */
	public Contato atualiza(Contato contatoOriginal, Contato contatoComInformacoesDeRequisicao) {
		contatoOriginal.setEmail(contatoComInformacoesDeRequisicao.getEmail());
		contatoOriginal.setNome(contatoComInformacoesDeRequisicao.getNome());
		contatoOriginal.setTelefone(contatoComInformacoesDeRequisicao.getTelefone());
		contatoOriginal.setDataNascimento(contatoComInformacoesDeRequisicao.getDataNascimento());

		Contato contatoAtualizado = contatoRepositorio.saveAndFlush(contatoOriginal);

		return contatoAtualizado;
	}

	/**
	 * Método do serviço que apaga o contao do banco através do id
	 * 
	 */
	public void deletaContatoPorId(Long id) {

		contatoRepositorio.deleteById(id);
	}

}
