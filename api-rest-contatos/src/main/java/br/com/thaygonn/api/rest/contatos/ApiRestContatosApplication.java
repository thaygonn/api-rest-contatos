package br.com.thaygonn.api.rest.contatos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Classe principal. gerenciada e chamada por primeiro pelo Spring framework
 * 
 * @author Thaygonn
 *
 */
@EnableWebMvc
@SpringBootApplication
public class ApiRestContatosApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiRestContatosApplication.class, args);
	}

}
