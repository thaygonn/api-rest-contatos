package br.com.thaygonn.api.rest.contatos.controlador;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.thaygonn.api.rest.contatos.modelo.Contato;
import br.com.thaygonn.api.rest.contatos.servico.ContatoServico;
import lombok.extern.slf4j.Slf4j;

/**
 * Classe responsável por orquestrar as chamadas entre a camada de modelo(Model)
 * e de visualização(View) da API
 * 
 * @author Thaygonn
 *
 */
@RestController
@RequestMapping("/api-contatos/contatos")
@Slf4j
public class ContatoControlador {

	@Autowired
	private ContatoServico contatoServico;

	/**
	 * Método que busca por todos os contatos
	 * 
	 * @return ResponseEntity<List<Contato>>
	 */
	@GetMapping
	public ResponseEntity<List<Contato>> buscaTodosContatos() {
		if (contatoServico.buscaTodosContatos().isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		log.info(contatoServico.buscaTodosContatos() + "");
		return ResponseEntity.ok(contatoServico.buscaTodosContatos());
	}

	/**
	 * Método que busca contato por ID
	 * 
	 * @return ResponseEntity<Contato>
	 */
	@GetMapping(path = "/{id}", produces = { "application/json" })
	public ResponseEntity<Contato> buscaContatoPorId(@PathVariable Long id) {
		if (contatoServico.buscaContatoPorId(id).isPresent()) {
			log.info(contatoServico.buscaContatoPorId(id) + "");
			return ResponseEntity.ok(contatoServico.buscaContatoPorId(id).get());

		}
		return ResponseEntity.notFound().build();
	}

	/**
	 * Método que busca todos os contatos pelo mesmo nome
	 * 
	 * @return ResponseEntity<List<Contato>>
	 */
	@GetMapping(path = "/nome", produces = { "application/json" })
	public ResponseEntity<List<Contato>> buscaContatoPorNome(@RequestParam String nome) {
		if (!contatoServico.buscaContatoPorNome(nome).isEmpty()) {

			return ResponseEntity.ok(contatoServico.buscaContatoPorNome(nome));

		}
		return ResponseEntity.notFound().build();
	}

	/**
	 * Método que adiciona um contato
	 * 
	 * @param contato
	 * @return ResponseEntity<Contato>
	 */
	@PostMapping
	@ResponseBody
	public ResponseEntity<Contato> adicionaContato(@RequestBody @Validated Contato contato) {
		try {

			URI uri = ServletUriComponentsBuilder.fromCurrentRequest().build().toUri();

			Contato contatoAdicionado = contatoServico.adiciona(contato);
			return ResponseEntity.created(uri).body(contatoAdicionado);

		} catch (ConstraintViolationException e) {
			log.error("Contato enviado já existe!  " + e);
			return ResponseEntity.badRequest().build();
		} catch (Exception e) {
			log.error("Request enviado está com problema. " + e);
			return ResponseEntity.badRequest().body(null);
		}
	}

	/**
	 * Método que atualiza o contato
	 * 
	 * @param id
	 * @param contato
	 * @return ResponseEntity<Contato>
	 */
	@PutMapping(path = "/{id}", produces = { "application/json" })
	public ResponseEntity<Contato> atualizaContato(@PathVariable("id") long id,
			@RequestBody @Validated Contato contato) {
		try {
			Optional<Contato> contatoOptional = contatoServico.buscaContatoPorId(id);

			if (contatoOptional.isPresent()) {

				Contato contatoAtualizado = contatoServico.atualiza(contatoOptional.get(), contato);

				return ResponseEntity.ok(contatoAtualizado);
			}
			log.error("Contato não encontrado.");
			return ResponseEntity.notFound().build();

		} catch (Exception e) {
			log.error("Request enviado está com problema." + e);
			return ResponseEntity.badRequest().body(null);
		}
	}

	/**
	 * Método que apaga contato
	 * 
	 * @param id
	 * @return ResponseEntity<Boolean>
	 */
	@DeleteMapping(path = "/{id}", produces = { "application/json" })
	public ResponseEntity<Boolean> apagaContato(@PathVariable("id") long id) {
		try {
			contatoServico.deletaContatoPorId(id);
			return ResponseEntity.ok().build();
		} catch (EmptyResultDataAccessException e) {
			log.error(e + "");
			return ResponseEntity.notFound().build();
		} catch (Exception e) {
			log.error(e + "");
			return ResponseEntity.badRequest().build();
		}
	}

}
